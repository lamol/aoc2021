# AOC 2021, Problem 10

Solution description is available at [AOC website](https://adventofcode.com/2021/day/10).

## How to build

Cmake is used to allow for cross-platform build, minimum version is given as 3.2, but it can be most likely downgraded, as no recent features are used for generation.

To build, run
* $ mkdir build && cd build && cmake .. && make

## How to run

For simplicity, the input is fed via stdin. To feed it from input file, run
* $ ./AOC\_problem10[.exe] <input\_file
No logger is used. All debug printouts are directly sent to stderr, for the sake of simplicity. To get rid of debug printouts and only get the answers, run as follows:
* $ ./AOC\_problem10[.exe] [<input\_file] 2>/dev/null
