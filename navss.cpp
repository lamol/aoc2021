#include <iostream>

#include "navss.h"

static bool is_open_bracket(char bracket) {
    return (bracket == '{' || bracket == '[' || bracket == '(' || bracket == '<');
}

static bool is_closing_bracket(char bracket) {
    return (bracket == '}' || bracket == ']' || bracket == ')' || bracket == '>');
}

static char get_closing_bracket(char bracket) {
    switch (bracket) {
    case '{':
	return '}';
    case '[':
	return ']';
    case '(':
	return ')';
    case '<':
	return '>';
    default:
	return '0';
    }
}

static int miss_score(char bracket) {
    switch (bracket) {
    case ')':
	return 3;
    case ']':
	return 57;
    case '}':
	return 1197;
    case '>':
	return 25137;
    default:
	return 0;
    }
}

static int match_score(char bracket) {
    switch (bracket) {
    case '(':
	return 1;
    case '[':
	return 2;
    case '{':
	return 3;
    case '<':
	return 4;
    default:
	return 0;
    }
}

int NavSubSystem::syntax_check(const std::string& line) {
    for (char bracket: line) {
	if (is_open_bracket(bracket)) {
	    this->bracket_tracker.push(bracket);
	} else if (is_closing_bracket(bracket)) {
	    char open_bracket = this->bracket_tracker.top();
	    this->bracket_tracker.pop();
	    if (get_closing_bracket(open_bracket) != bracket) {
		return miss_score(bracket);
	    }
	} else if (!isspace(bracket)) {
	    std::cerr << "Unexpected input:" << bracket << std::endl;
	}
    }
    return 0;
}

long long NavSubSystem::count_autocomplete_score() {
    long long score = 0;
    while (!bracket_tracker.empty()) {
	char bracket = bracket_tracker.top();
        long long add_score = match_score(bracket);
	score = score * 5 + add_score;
	std::cerr << "Bracket:" << bracket << ",Score:" << score << std::endl;
	bracket_tracker.pop();
    }
    return score;
}
