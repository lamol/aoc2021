#ifndef NAVSS_H_
#define NAVSS_H_

#include <stack>
#include <string>

class NavSubSystem {
 private:
    std::stack<char> bracket_tracker;
 public:
    NavSubSystem(): bracket_tracker() {}
    int syntax_check(const std::string&);
    long long count_autocomplete_score();
};

#endif
