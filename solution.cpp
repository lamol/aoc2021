#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

#include "navss.h"

int main() {
    int error_score = 0;
    std::vector<long long> match_scores{};

    for (std::string chunk; std::getline(std::cin, chunk); ) {
	NavSubSystem navss{};
	int line_score = navss.syntax_check(chunk);
	std::cerr << "Chunk:" << chunk << std::endl;
	std::cerr << "Line error score:" << line_score << std::endl;
	error_score += line_score;
	if (line_score == 0) {
	    long long match_score = navss.count_autocomplete_score();
	    std::cerr << "Line autocomplete score:" << match_score << std::endl;
	    match_scores.push_back(match_score);
	}
    }

    size_t median_idx = match_scores.size() / 2;
    std::nth_element(match_scores.begin(),
		     match_scores.begin() + median_idx,
		     match_scores.end());
    long long second_answer = match_scores[median_idx];

    std::cout << "First part score:" << error_score << std::endl;
    std::cout << "Second part answer:" << second_answer << std::endl;
    return 0;
}
